import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Technician, Appointment
from django.forms.models import model_to_dict
from .encoders import (
    AppointmentDetailEncoder,
    AppointmentEncoder,
    TechnicianEncoder,
)


@require_http_methods(["GET", "POST"])
def api_appointment_list(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments}, encoder=AppointmentEncoder, safe=False
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        if "employee_id" not in content:
            return JsonResponse({"message": "Employee ID is required"}, status=400)
        employee_id = content["employee_id"]
        try:
            technician = Technician.objects.get(employee_id=employee_id)
        except Technician.DoesNotExist:
            return JsonResponse({"message": "No technician found"})

        appointment = Appointment.objects.create(
            date_time=content["date_time"],
            reason=content["reason"],
            vin=content["vin"],
            customer=content["customer"],
            technician=technician,
        )
        appointment_dict = model_to_dict(appointment)
        return JsonResponse(
            appointment_dict, encoder=AppointmentDetailEncoder, safe=False
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_appointment_detail(request, id):
    if request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    elif request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(appointment, encoder=AppointmentDetailEncoder, safe=False)
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "technician" in content:
                technician = Technician.objects.get(employee_id=content["technician"])
                content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse({"message": "No technician found"})
        Appointment.objects.filter(id=id).update(**content)
        return JsonResponse({"message": "Appointment updated successfully"})


@require_http_methods(["GET", "POST"])
def api_technician_list(request):
    if request.method == "GET":
        technician = Technician.objects.all()
        return JsonResponse(
            {"technician": technician}, encoder=TechnicianEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)
        except Exception as e:
            return JsonResponse({"message": "Error creating technician:" + str(e)})
