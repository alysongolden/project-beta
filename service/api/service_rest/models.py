from django.db import models


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=20)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=30)
    employee_id = models.CharField(max_length=20)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField()
    status = models.CharField(max_length=20)
    vin = models.CharField(max_length=20)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician, related_name="technician", on_delete=models.CASCADE
    )
