from django.urls import path
from .views import api_technician_list, api_appointment_list, api_appointment_detail


urlpatterns = [
   path('technicians/', api_technician_list, name='api_technician_list'),
   path('appointments/', api_appointment_list, name='api_appointment_list'),
   path('appointments/new/', api_appointment_list, name='api_appointment_list'),
   path('appointments/<int:id>/', api_appointment_detail, name='api_appointment_detail'),
   path('appointments/<int:id>/finish/', api_appointment_detail, name='api_appointment_detail'),
   path('appointments/<int:id>/cancel/', api_appointment_detail, name='api_appointment_detail'),
]
