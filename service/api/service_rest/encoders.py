from .models import AutomobileVO, Technician, Appointment
from common.json import ModelEncoder


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
        "manufacturer",
        "model",
        "color",
        "year",
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "vin",
        "customer",
        "status",
        "technician",
        "id",
    ]
    encoders = {"technician": TechnicianEncoder()}


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "vin",
        "customer",
        "status",
        "technician",
        "id",
    ]
    encoders = {"technician": TechnicianEncoder()}
