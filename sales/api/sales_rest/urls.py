from django.urls import path

from .views import (
    list_or_create_salespeople,
    delete_salesperson,
    list_or_create_customers,
    delete_customer,
    list_or_create_sales,
    delete_sale,
)

urlpatterns = [
    path("salespeople/", list_or_create_salespeople, name="list_or_create_salespeople"),
    path("salespeople/<int:id>/", delete_salesperson, name="delete_salesperson"),
    path("salespeople/new/", list_or_create_salespeople, name="new_salesperson"),
    path("customers/", list_or_create_customers, name="list_or_create_customers"),
    path("customers/<int:id>/", delete_customer, name="delete_customer"),
    path("customers/new/", list_or_create_customers, name="new_customers"),
    path("sales/", list_or_create_sales, name="list_or_create_sales"),
    path("sales/<int:id>/", delete_sale, name="delete_sale"),
    path("sales/new/", list_or_create_sales, name="new_sale"),
]
