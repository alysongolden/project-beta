from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return f"{self.vin}"


class Salesperson(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=30)
    employee_id = models.CharField(max_length=20)


class Customer(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=30)
    address = models.TextField()
    phone_number = models.CharField(max_length=20)


class Sale(models.Model):
    automobile = models.ForeignKey(
        "AutomobileVO", related_name="sales", on_delete=models.CASCADE
    )
    salesperson = models.ForeignKey(
        "Salesperson", related_name="sales", on_delete=models.CASCADE
    )
    customer = models.ForeignKey(
        "Customer", related_name="sales", on_delete=models.CASCADE
    )
    price = models.DecimalField(max_digits=10, decimal_places=2)
