import { useEffect, useState } from "react";

function AppointmentList() {
  const [appointments, setAppointments] = useState([]);
  const [filteredAppointments, setFilteredAppointments] = useState([]);

  useEffect(() => {
    const fetchAppointments = async () => {
      try {
        const response = await fetch("http://localhost:8080/api/appointments/");

        if (!response.ok) {
          console.error("Failed to fetch data");
          return;
        }

        const data = await response.json();
        setAppointments(data.appointments);
        setFilteredAppointments(
          data.appointments.filter((appointment) => appointment.status === "")
        );
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchAppointments();
  }, []);

  const getStatusLabel = (status) => {
    return status === "" ? "Scheduled" : status;
  };

  const cancelAppointment = async (id) => {
    try {
      const response = await fetch(
        `http://localhost:8080/api/appointments/${id}/cancel/`,
        {
          method: "PUT",
          body: JSON.stringify({
            status: "Canceled",
          }),
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      if (response.ok) {
        setFilteredAppointments(
          filteredAppointments.filter((appointment) => appointment.id != id)
        );
      } else {
        console.error("Failed to cancel appointment");
      }
    } catch (error) {
      console.error("Error canceling appointment:", error);
    }
  };

  const finishAppointment = async (id) => {
    try {
      const response = await fetch(
        `http://localhost:8080/api/appointments/${id}/finish/`,
        {
          method: "PUT",
          body: JSON.stringify({
            status: "Finished",
          }),
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      if (response.ok) {
        setFilteredAppointments(
          filteredAppointments.filter((appointment) => appointment.id != id)
        );
      } else {
        console.error("Failed to finish appointment");
      }
    } catch (error) {
      console.error("Error finishing appointment:", error);
    }
  };

  return (
    <div>
      <h2>Appointments</h2>
      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">Appointment Date</th>
            <th scope="col">Customer</th>
            <th scope="col">Vin</th>
            <th scope="col">Reason</th>
            <th scope="col">Status</th>
            <th scope="col">Technician</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>
          {filteredAppointments.map((appointment) => (
            <tr key={appointment.id}>
              <td>{appointment.date_time}</td>
              <td>{appointment.customer}</td>
              <td>{appointment.vin}</td>
              <td>{appointment.reason}</td>
              <td>{getStatusLabel(appointment.status)}</td>
              <td>{appointment.technician.employee_id}</td>
              <td>
                {appointment.status === "" && (
                  <button
                    className="btn btn-outline-danger"
                    onClick={() => cancelAppointment(appointment.id)}
                  >
                    Cancel
                  </button>
                )}
                {appointment.status === "" && (
                  <button
                    className="btn btn-outline-success"
                    onClick={() => finishAppointment(appointment.id)}
                  >
                    Finish
                  </button>
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentList;
