import React, { useState, useEffect } from "react";

function ModelsForm() {
  const [formData, setFormData] = useState({
    name: "",
    picture_url: "",
    manufacturer_id: "",
  });
  const [errors, setErrors] = useState({});
  const [manufacturers, setManufacturers] = useState([]);

  const fetchManufacturers = async () => {
    try {
      const response = await fetch("http://localhost:8100/api/manufacturers/");
      if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
      } else {
        console.error("Error fetching manufacturers");
      }
    } catch (error) {
      console.error("Error fetching manufacturers:", error);
    }
  };

  useEffect(() => {
    fetchManufacturers();
  }, []);

  const handleFormChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    try {
      const response = await fetch("http://localhost:8100/api/models/", {
        method: "POST",
        body: JSON.stringify(formData),
        headers: {
          "Content-Type": "application/json",
        },
      });

      if (response.ok) {
        const newModel = await response.json();
        setFormData({
          name: "",
          picture_url: "",
          manufacturer_id: "",
        });
      } else {
        console.error("Unsuccessful, please try again:", response.status);
      }
    } catch (error) {
      console.error("Error creating model:", error);
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create Model</h1>
          <form onSubmit={handleSubmit} id="model-form">
            <div className="form-floating mb-3">
              <input
                value={formData.name}
                onChange={handleFormChange}
                name="name"
                placeholder="Model Name"
                type="text"
                id="name"
                className="form-control"
                required
              />
              <label htmlFor="name">Model Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.picture_url}
                onChange={handleFormChange}
                name="picture_url"
                placeholder="Picture URL"
                type="url"
                id="picture_url"
                className="form-control"
                required
              />
              <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="form-floating mb-3">
              <select
                value={formData.manufacturer_id}
                onChange={handleFormChange}
                name="manufacturer_id"
                id="manufacturer_id"
                className="form-select"
                required
              >
                <option value="">Select Manufacturer</option>
                {manufacturers.map((manufacturer) => (
                  <option key={manufacturer.id} value={manufacturer.id}>
                    {manufacturer.name}
                  </option>
                ))}
              </select>
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="text-center">
              <button type="submit" className="btn btn-outline-secondary">
                Create
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ModelsForm;
