import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./index.css";
import MainPage from "./MainPage";
import Nav from "./Nav";
import ManufacturersList from "./ManufacturersList";
import ManufacturerForm from "./ManufacturersForm";
import SalespeopleList from "./SalespeopleList";
import SalespersonForm from "./SalespersonForm";
import CustomerList from "./CustomerList";
import CustomerForm from "./CustomerForm";
import SaleList from "./SalesList";
import SaleForm from "./SalesForm";
import SaleHistory from "./SalesHistory";
import TechnicianList from "./TechnicianList";
import TechnicianForm from "./TechnicianForm";
import AppointmentList from "./AppointmentList";
import AppointmentForm from "./AppointmentForm";
import AutomobileList from "./AutomobileList";
import AutomobileForm from "./AutomobileForm";
import ModelsForm from "./ModelsForm";
import ModelsList from "./ModelsList";
import ServiceHistoryList from "./ServiceHistoryList";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers" element={<ManufacturersList />} />
          <Route path="/manufacturers/new" element={<ManufacturerForm />} />
          <Route path="/salespeople" element={<SalespeopleList />} />
          <Route path="/salespeople/new" element={<SalespersonForm />} />
          <Route path="/customers" element={<CustomerList />} />
          <Route path="/customers/new" element={<CustomerForm />} />
          <Route path="/sales" element={<SaleList />} />
          <Route path="/sales/new" element={<SaleForm />} />
          <Route path="/sales/history" element={<SaleHistory />} />
          <Route path="/technicians/" element={<TechnicianList />} />
          <Route path="/technicians/new/" element={<TechnicianForm />} />
          <Route path="/appointments/" element={<AppointmentList />} />
          <Route path="/appointments/new/" element={<AppointmentForm />} />
          <Route path="/automobiles/" element={<AutomobileList />} />
          <Route
            path="/appointments/history/"
            element={<ServiceHistoryList />}
          />
          <Route path="/automobiles/new/" element={<AutomobileForm />} />
          <Route path="/models/" element={<ModelsList />} />
          <Route path="/models/new/" element={<ModelsForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
