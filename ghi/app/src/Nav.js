import { NavLink } from "react-router-dom";
import "./index.css";

function Nav() {
	return (
		<nav className="navbar navbar-expand-lg navbar-dark ">
			<div className="container-fluid">
				<NavLink className="navbar-brand" to="/">
					CarCar
				</NavLink>
				<button
					className="navbar-toggler"
					type="button"
					data-bs-toggle="collapse"
					data-bs-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent"
					aria-expanded="false"
					aria-label="Toggle navigation"
				>
					<span className="navbar-toggler-icon"></span>
				</button>
				<div className="collapse navbar-collapse" id="navbarSupportedContent">
					<ul className="navbar-nav me-auto mb-2 mb-lg-0">
						<li className="nav-item dropdown">
							<NavLink
								className="nav-link dropdown-toggle"
								to="#"
								id="navbarDropdown"
								role="button"
								data-bs-toggle="dropdown"
								aria-expanded="false"
							>
								Manufacturers
							</NavLink>
							<ul className="dropdown-menu" aria-labelledby="navbarDropdown">
								<li>
									<NavLink className="dropdown-item" to="/manufacturers">
										List of Manufacturers
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/manufacturers/new">
										Add a Manufacturer
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<NavLink
								className="nav-link dropdown-toggle"
								to="#"
								id="navbarDropdown"
								role="button"
								data-bs-toggle="dropdown"
								aria-expanded="false"
							>
								Models
							</NavLink>
							<ul className="dropdown-menu" aria-labelledby="navbarDropdown">
								<li>
									<NavLink className="dropdown-item" to="/models">
										List of Models
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/models/new">
										Add a Model
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<NavLink
								className="nav-link dropdown-toggle"
								to="#"
								id="navbarDropdown"
								role="button"
								data-bs-toggle="dropdown"
								aria-expanded="false"
							>
								Automobiles
							</NavLink>
							<ul className="dropdown-menu" aria-labelledby="navbarDropdown">
								<li>
									<NavLink className="dropdown-item" to="/automobiles">
										List of Automobiles
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/automobiles/new">
										Add a Automobile
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<NavLink
								className="nav-link dropdown-toggle"
								to="#"
								id="navbarDropdown"
								role="button"
								data-bs-toggle="dropdown"
								aria-expanded="false"
							>
								Salespeople
							</NavLink>
							<ul className="dropdown-menu" aria-labelledby="navbarDropdown">
								<li>
									<NavLink className="dropdown-item" to="/salespeople">
										Salespeople List
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/salespeople/new">
										Add a Salesperson
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<NavLink
								className="nav-link dropdown-toggle"
								to="#"
								id="navbarDropdown"
								role="button"
								data-bs-toggle="dropdown"
								aria-expanded="false"
							>
								Customers
							</NavLink>
							<ul className="dropdown-menu" aria-labelledby="navbarDropdown">
								<li>
									<NavLink className="dropdown-item" to="/customers">
										Customer List
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/customers/new">
										Add a Customer
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<NavLink
								className="nav-link dropdown-toggle"
								to="#"
								id="navbarDropdown"
								role="button"
								data-bs-toggle="dropdown"
								aria-expanded="false"
							>
								Sales
							</NavLink>
							<ul className="dropdown-menu" aria-labelledby="navbarDropdown">
								<li>
									<NavLink className="dropdown-item" to="/sales">
										Sales List
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/sales/new">
										Add a Sale
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/sales/history">
										Sales History
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<NavLink
								className="nav-link dropdown-toggle"
								to="#"
								id="navbarDropdown"
								role="button"
								data-bs-toggle="dropdown"
								aria-expanded="false"
							>
								Technicians
							</NavLink>
							<ul className="dropdown-menu" aria-labelledby="navbarDropdown">
								<li>
									<NavLink className="dropdown-item" to="/technicians">
										Technicians List
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/technicians/new">
										Add a Technician
									</NavLink>
								</li>
							</ul>
						</li>
						<li className="nav-item dropdown">
							<NavLink
								className="nav-link dropdown-toggle"
								to="#"
								id="navbarDropdown"
								role="button"
								data-bs-toggle="dropdown"
								aria-expanded="false"
							>
								Service Appointments
							</NavLink>
							<ul className="dropdown-menu" aria-labelledby="navbarDropdown">
								<li>
									<NavLink className="dropdown-item" to="/appointments">
										Service Appointments List
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/appointments/new">
										Create a Service Appointment
									</NavLink>
								</li>
								<li>
									<NavLink className="dropdown-item" to="/appointments/history">
										Service History
									</NavLink>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	);
}

export default Nav;
