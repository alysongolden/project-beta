import React, { useState, useEffect } from "react";

function CustomerList() {
	const [customers, setCustomers] = useState([]);

	useEffect(() => {
		const fetchCustomers = async () => {
			const response = await fetch("http://localhost:8090/api/customers/");
			if (response.ok) {
				const data = await response.json();
				setCustomers(data);
			} else {
				console.error("Error fetching customers:", response.statusText);
			}
		};

		fetchCustomers();
	}, []);

	return (
		<div>
			<h1>Customers</h1>
			<table className="table table-hover table-striped">
				<thead>
					<tr>
						<th scope="col">First Name</th>
						<th scope="col">Last Name</th>
						<th scope="col">Phone Number</th>
						<th scope="col">Address</th>
					</tr>
				</thead>
				<tbody>
					{customers.map((customer) => (
						<tr key={customer.id}>
							<th scope="row">{customer.first_name}</th>
							<td>{customer.last_name}</td>
							<td>{customer.phone_number}</td>
							<td>{customer.address}</td>
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);
}

export default CustomerList;
