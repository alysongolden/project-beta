import React, { useEffect, useState } from "react";

function TechnicianForm() {
  const [technicians, setTechnicians] = useState([]);
  const [formData, setFormData] = useState({
    first_name: "",
    last_name: "",
    employee_id: "",
  });

  const handleFormChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const technicianUrl = "http://localhost:8080/api/technicians/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(technicianUrl, fetchConfig);
    if (response.ok) {
      const newTechnician = await response.json();
      setFormData({
        first_name: "",
        last_name: "",
        employee_id: "",
      });
    } else {
      console.error("Unsuccessful, please try again:", response.status);
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create New Technician</h1>
          <form onSubmit={handleSubmit} id="technician-form">
            <div className="form-floating mb-3">
              <input
                value={formData.first_name}
                onChange={handleFormChange}
                name="first_name"
                placeholder="First Name"
                type="text"
                id="first_name"
                className="form-control"
                required
              />
              <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.last_name}
                onChange={handleFormChange}
                name="last_name"
                placeholder="Last Name"
                type="text"
                id="last_name"
                className="form-control"
                required
              />
              <label htmlFor="last_name">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.employee_id}
                onChange={handleFormChange}
                name="employee_id"
                placeholder="Employee ID"
                type="text"
                id="employee_id"
                className="form-control"
                required
              />
              <label htmlFor="employee_id">Employee ID</label>
            </div>
            <div className="text-center">
              <button type="submit" className="btn btn-outline-secondary">
                Create
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default TechnicianForm;
