import React, { useState, useEffect } from "react";

function SalesHistory() {
	const [salespersonId, setSalespersonId] = useState("");
	const [salesHistory, setSalesHistory] = useState([]);
	const [salespeople, setSalespeople] = useState([]);

	useEffect(() => {
		const fetchSalespeople = async () => {
			const response = await fetch("http://localhost:8090/api/salespeople/");
			if (response.ok) {
				const data = await response.json();
				setSalespeople(data);
			} else {
				console.error("Error fetching salespeople:", response.statusText);
			}
		};
		fetchSalespeople();
	}, []);

	useEffect(() => {
		const fetchSalesHistory = async () => {
			if (salespersonId) {
				const salesResponse = await fetch("http://localhost:8090/api/sales/");
				if (salesResponse.ok) {
					const allSalesData = await salesResponse.json();
					const filteredSales = allSalesData.filter(
						(sale) => String(sale.salesperson.id) === salespersonId
					);
					setSalesHistory(filteredSales);
				} else {
					console.error(
						"Error fetching sales history:",
						salesResponse.statusText
					);
				}
			} else {
				setSalesHistory([]);
			}
		};

		fetchSalesHistory();
	}, [salespersonId]);

	return (
		<div>
			<h1>Sales History</h1>
			<div>
				<select
					name="salesperson"
					value={salespersonId}
					onChange={(e) => setSalespersonId(e.target.value)}
					className="custom-dropdown"
				>
					<option value="">Select a salesperson</option>
					{salespeople.map((salesperson) => (
						<option key={salesperson.id} value={salesperson.id}>
							{salesperson.first_name} {salesperson.last_name}
						</option>
					))}
				</select>
			</div>
			{salesHistory.length > 0 ? (
				<table className="table table-hover table-striped">
					<thead>
						<tr>
							<th scope="col">Salesperson </th>
							<th scope="col">Customer</th>
							<th scope="col">VIN</th>
							<th scope="col">Price</th>
						</tr>
					</thead>
					<tbody>
						{salesHistory.map((sale) => (
							<tr key={sale.id}>
								<td>
									{sale.salesperson.first_name} {sale.salesperson.last_name}
								</td>
								<td>
									{sale.customer.first_name} {sale.customer.last_name}
								</td>
								<td>{sale.automobile.vin}</td>
								<td>
									{sale.price.toLocaleString("en-US", {
										style: "currency",
										currency: "USD",
									})}
								</td>
							</tr>
						))}
					</tbody>
				</table>
			) : (
				<p>No sales history found for the selected salesperson.</p>
			)}
		</div>
	);
}

export default SalesHistory;
