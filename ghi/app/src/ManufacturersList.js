import React, { useState, useEffect } from "react";

function ManufacturersList() {
	const [manufacturers, setManufacturers] = useState([]);

	useEffect(() => {
		const fetchManufacturers = async () => {
			const response = await fetch("http://localhost:8100/api/manufacturers/");
			if (response.ok) {
				const data = await response.json();
				setManufacturers(data.manufacturers);
			} else {
				console.error("Error fetching manufacturers:", response.statusText);
			}
		};

		fetchManufacturers();
	}, []);

	return (
		<div>
			<h1>Manufacturers</h1>
			<table className="table table-hover table-striped">
				<thead>
					<tr>
						<th scope="col">Name</th>
					</tr>
				</thead>
				<tbody>
					{manufacturers.map((manufacturer) => (
						<tr key={manufacturer.id}>
							<th scope="row">{manufacturer.name}</th>
						</tr>
					))}
				</tbody>
			</table>
		</div>
	);
}

export default ManufacturersList;
