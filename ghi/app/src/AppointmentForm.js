import React, { useEffect, useState } from "react";

function AppointmentForm() {
  const [appointments, setAppointments] = useState([]);
  const [formData, setFormData] = useState({
    date_time: "",
    reason: "",
    vin: "",
    customer: "",
    employee_id: "",
  });

  const handleFormChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    const appointmentUrl = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(appointmentUrl, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      setFormData({
        date_time: "",
        reason: "",
        vin: "",
        customer: "",
        employee_id: "",
      });
    } else {
      console.error("Unsuccessful, please try again:", response);
    }
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Schedule a service appointment</h1>
          <form onSubmit={handleSubmit} id="appointment-form">
            <div className="form-floating mb-3">
              <input
                value={formData.date_time}
                onChange={handleFormChange}
                name="date_time"
                placeholder="Date"
                type="datetime-local"
                id="date_time"
                className="form-control"
                required
              />
              <label htmlFor="date_time">Date</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.reason}
                onChange={handleFormChange}
                name="reason"
                placeholder="Reason"
                type="text"
                id="reason"
                className="form-control"
                required
              />
              <label htmlFor="reason">Reason</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.vin}
                onChange={handleFormChange}
                name="vin"
                placeholder="VIN"
                type="text"
                id="vin"
                className="form-control"
                required
              />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.customer}
                onChange={handleFormChange}
                name="customer"
                placeholder="Customer"
                type="text"
                id="customer"
                className="form-control"
                required
              />
              <label htmlFor="customer">Customer Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                value={formData.employee_id}
                onChange={handleFormChange}
                name="employee_id"
                placeholder="Technician"
                type="text"
                id="employee_id"
                className="form-control"
                required
              />
              <label htmlFor="employee_id">Technician</label>
            </div>
            <div className="text-center">
              <button type="submit" className="btn btn-outline-secondary">
                Schedule
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AppointmentForm;
