import { useEffect, useState } from "react";

function ModelsList() {
  const [models, setModels] = useState([]);

  useEffect(() => {
    const fetchModels = async () => {
      const response = await fetch("http://localhost:8100/api/models/");
      if (response.ok) {
        const data = await response.json();
        setModels(data.models);
      } else {
        console.error("Cannot retrieve models:", response.statusText);
      }
    };
    fetchModels();
  }, []);

  return (
    <div>
      <h2>Models</h2>
      <table className="table table-hover">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Manufacturer</th>
            <th scope="col">Photo</th>
          </tr>
        </thead>
        <tbody>
          {models.map((model) => (
            <tr key={model.id}>
              <th scope="row">{model.name}</th>
              <td>{model.manufacturer.name}</td>
              <td>
                <img
                  src={model.picture_url}
                  alt={model.name}
                  style={{ maxWidth: "300px", maxHeight: "300px" }}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default ModelsList;
