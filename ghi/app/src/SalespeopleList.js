import React, { useState, useEffect } from 'react';

function SalespeopleList() {
  const [salespeople, setSalespeople] = useState([]);

  useEffect(() => {
    const fetchSalespeople = async () => {
      const response = await fetch('http://localhost:8090/api/salespeople/');
      if (response.ok) {
        const data = await response.json();
        setSalespeople(data);
      } else {
        console.error('Error fetching salespeople:', response.statusText);
      }
    };
    fetchSalespeople();
  }, []);

  return (
    <div>
      <h1>Salespeople</h1>
      <table className="table table-hover table-striped">
        <thead>
          <tr>
            <th scope="col">Employee ID</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
          </tr>
        </thead>
        <tbody>
          {salespeople.map((person) => (
            <tr key={person.id}>
              <th scope="row">{person.employee_id}</th>
              <td>{person.first_name}</td>
              <td>{person.last_name}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

  export default SalespeopleList;
