# CarCar

Team:

* Alyson - Sales, Manufacturers, and Readme
* Lauren - Service, Models, and Automobiles

## Steps on How to Setup and Run the Project

* Fork the repository at https://gitlab.com/alysongolden/project-beta
* Clone the repository using "git clone" and the link under the blue code button in the new Project Beta under your fork. The link will be under "Clone with HTTPS"
* Create database and build images and containers:
    docker volume create beta-data
    docker-compose build
    docker-compose up
* run at localhost:3000/

## Design

![alt text](image.png))
 - the value object in this project: AutomobileVO
 - the bounded contexts in this project: Sales, Service, and Inventory.

## Service microservice

My service models (AutomobileVO, Technician, and Appointment), include components to do with viewing and creating technicians, as well as scheduling appointments for customers. The appointment form is including their VIN, reason, name, technician assigned and its status which can be changed from the appointment list page to cancel or finish the appointment which then moves it to the service history page with 'finished' or 'canceled' status. The AutomobileVO handles the VIN and sold aspects which pull from the inventory database.

- Service ports and JSON information (port :8080)

-- List technicians	- GET
http://localhost:8080/api/technicians/

---the expected output---

{
	"technician": [
		{
			"first_name": "",
			"last_name": "",
			"employee_id": ""
		},
]}


-- Create a technician	- POST
http://localhost:8080/api/technicians/

{
    "first_name": "",
    "last_name": "",
    "employee_id": ""
}

---the expected output---

{
	"technician": {
		"first_name": "",
		"last_name": "",
		"employee_id": ""
	}
}

-- List appointments - GET
http://localhost:8080/api/appointments/

---the expected output---


--Create an appointment - POST
http://localhost:8080/api/appointments/

{
	"date_time": "",
	"reason": "",
	"vin": "",
	"customer": "",
	"employee_id": ""
}

---the expected output---

{
	"id": #,
	"date_time": "0000-00-00 00:00:00",
	"reason": "",
	"status": "",
	"vin": "",
	"customer": "",
	"technician": #
}

--Set appointment status to "canceled"	- PUT
http://localhost:8080/api/appointments/:id/cancel/

{
	"status": "Canceled"
}

--Set appointment status to "finished"	- PUT
http://localhost:8080/api/appointments/:id/finish/

{
	"status": "Finished"
}

## Sales microservice

I took the model for the Automobile in the Inventory API and looked at all the characteristics of the model. I found that what would be needed for the Automobile to be successful in my Sales API I would only be needing the VIN, sold status, and the import_href.  My models inside the Sales microservice are Customer, Sale, Salesperson, and AutomobileVO. Carrying the VIN argument over would allow to show sales data by the VIN number and a way to individually call the Automobiles. The sold argument allows a sale to be created and then changing the sold status in the database to keep cars from being sold twice.

- Sales ports and JSON information (port :8090)

-- List salespeople	- GET
http://localhost:8090/api/salespeople/

---the expected output---

	{
		"id": #,
		"first_name": "",
		"last_name": "",
		"employee_id": ""
	},


-- Create a salesperson - POST
http://localhost:8090/api/salespeople/

{
    "first_name": "",
    "last_name": "",
    "employee_id": ""
}

---the expected output---

{
    "id": #,
    "first_name": "",
    "last_name": "",
    "employee_id": ""
}



-- List customers - GET
http://localhost:8090/api/customers/

---the expected output---

	{
		"id": #,
		"first_name": "",
		"last_name": "",
		"address": "",
		"phone_number": "000-0000"
	},


-- Create a customer - POST
http://localhost:8090/api/customers/

{
    "first_name": "",
    "last_name": "",
    "address": "",
    "phone_number": "000-0000"
}

---the expected output---

{
    "id": #,
    "first_name": "",
    "last_name": "",
    "address": "",
    "phone_number": "000-0000"
}

-- List sales - GET
http://localhost:8090/api/sales/

---the expected output---

	{
		"automobile": {
			"id": #,
			"vin": "",
			"sold": true,
			"import_href": "/api/automobiles/:vin/"
		},
		"salesperson": {
			"id": #,
			"first_name": "",
			"last_name": "",
			"employee_id": ""
		},
		"customer": {
			"id": #,
			"first_name": "",
			"last_name": "",
			"address": "",
			"phone_number": "000-0000"
		},
		"price": 00000.00
	},


-- Create a sale - POST
http://localhost:8090/api/sales/

{
    "automobile": {
        "vin": ""
    },
    "salesperson": {
        "first_name": "",
				"last_name": ""
    },
    "customer": {
        "first_name": "",
				"last_name": ""
    },
    "price": "00000.00"
}

---the expected output---


#################################
## INVENTORY RELATED ##
port :8100
#################################



## Manufacturers:
-- List manufacturers	- GET
http://localhost:8100/api/manufacturers/

-- Create a manufacturer	- POST
http://localhost:8100/api/manufacturers/

{
  "name": ""
}

-- Get a specific manufacturer	- GET
http://localhost:8100/api/manufacturers/:id/

-- Update a specific manufacturer	- PUT
http://localhost:8100/api/manufacturers/:id/

{
  "href": "/api/manufacturers/:id/",
  "id": #,
  "name": ""
}

-- Delete a specific manufacturer	- DELETE
http://localhost:8100/api/manufacturers/:id/

## Vehicle models:
-- List vehicle models	- GET
http://localhost:8100/api/models/

---the expected output---

{
	"models": [
		{
			"href": "/api/models/:id/",
			"id": #,
			"name": "",
			"picture_url": "",
			"manufacturer": {
				"href": "/api/manufacturers/:id/",
				"id": #,
				"name": ""
            }
        }
    ]
}



-- Create a vehicle model - POST
http://localhost:8100/api/models/

{
  "name": "",
  "picture_url": "",
  "manufacturer_id": #
}

---the expected output---

{
	"href": "/api/models/:id/",
	"id": #,
	"name": "",
	"picture_url": "",
	"manufacturer": {
		"href": "/api/manufacturers/:id/",
		"id": #,
		"name": ""
	}
}

-- Get a specific automobile - GET
http://localhost:8100/api/automobiles/:vin/

-- Update a specific automobile	- PUT
http://localhost:8100/api/automobiles/:vin/

{
  "name": "",
  "picture_url": ""
}


-- Delete a specific automobile	- DELETE
http://localhost:8100/api/automobiles/:vin/


## Automobile information:
-- List automobiles - GET
http://localhost:8100/api/automobiles/

---the expected output---

{
	"autos": [
        ## your automobiles will list here
    ]
}

-- Create an automobile - POST
http://localhost:8100/api/automobiles/

{
  "color": "",
  "year": ,
  "vin": "",
  "model_id": #
}

---the expected output---

{
	"href": "/api/automobiles/:vin/",
	"id": #,
	"color": "",
	"year": ,
	"vin": "",
	"model": {
		"href": "/api/models/:id/",
		"id": #,
		"name": "",
		"picture_url": "",
		"manufacturer": {
			"href": "/api/manufacturers/:id/",
			"id": #,
			"name": ""
		}
	},
	"sold": false
}

-- Get a specific automobile - GET
http://localhost:8100/api/automobiles/:vin/

---the expected output---

{
  "href": "/api/automobiles/:vin/",
  "id": #,
  "color": "",
  "year": ,
  "vin": "",
  "model": {
    "href": "/api/models/:id/",
    "id": #,
    "name": "",
    "picture_url": "",
    "manufacturer": {
      "href": "/api/manufacturers/:id/",
      "id": #,
      "name": ""
    }
  },
  "sold": false/true
}

-- Update a specific automobile - PUT
http://localhost:8100/api/automobiles/:vin/

{
  "name": "",
  "picture_url": ""}
{
  "color": "",
  "year": ,
  "sold":
}

-- Delete a specific automobile	- DELETE
http://localhost:8100/api/automobiles/:vin/
